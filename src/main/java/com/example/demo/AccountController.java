package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/account")
@RestController
public class AccountController {
	
	@Autowired
	AccountService accountservice;
	@GetMapping("/")
	void call() {
		System.out.println("account class");
	}
	
	
	@PostMapping
	private String saveAccountl(@RequestBody Account account) {
		accountservice.save(account);
		System.out.println("account details --- " + account.getName() + account.getAccountType() +" " +  account.getAddress()  +" " +  account.getBalance());
		return "post called";
	}
	
}

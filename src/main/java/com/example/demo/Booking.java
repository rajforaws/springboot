package com.example.demo;

public class Booking {

	String name;
	String email;
	int age;
	int totalSeatBooked;
	String gender;
	String meal;
	String seatNumber;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getTotalSeatBooked() {
		return totalSeatBooked;
	}
	public void setTotalSeatBooked(int totalSeatBooked) {
		this.totalSeatBooked = totalSeatBooked;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMeal() {
		return meal;
	}
	public void setMeal(String meal) {
		this.meal = meal;
	}
	public String getSeatNumber() {
		return seatNumber;
	}
	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}
	
	@Override
	public String toString() {
		return "Booking [name=" + name + ", email=" + email + ", age=" + age + ", totalSeatBooked=" + totalSeatBooked
				+ ", gender=" + gender + ", meal=" + meal + ", seatNumber=" + seatNumber + "]";
	}
	
}

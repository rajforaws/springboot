package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class UserController{
	
	@Autowired
	UserService userservice;
	@GetMapping("/")
	void getUsers() {
		System.out.println("Called");
	}
	
	@GetMapping("/{id}")
	void getUser(@PathVariable Integer id) {
		System.out.println("Called  ------- "+id );
	}
	
	@PostMapping
	private String saveUser(@RequestBody User user) {
		userservice.save(user);
		System.out.println("user name: " + user.getName()+", age:  " +user.getAge());
		return "post called";
	}
	
	
	
	@PutMapping
	private String putcall() {
		System.out.println("put");
		return "put called";
	}
	
	
	

}

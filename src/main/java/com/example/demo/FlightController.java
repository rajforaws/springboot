package com.example.demo;


import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/v1.0/flight")
@RestController
public class FlightController{
	
	@PostMapping("/airline/register")
	void registerUser(FlightUser flightuser) {
		System.out.println("add user"+ flightuser.getEmail()+ flightuser.getName());
	}
	
	@PostMapping("/admin/login")
	void adminLogin() {
		System.out.println("admion login successful");
	}
	
	@PostMapping("/airline/inventory/add")
	void addSchdule() {
		System.out.println("Schedule added");
	}
	
	@PostMapping("/search")
	void searchFlight() {
		System.out.println("flight searched Called");
	}
	
	
	@PostMapping("/booking/{flightid}")
	void BookTicket(@RequestBody Integer flightid) {
		System.out.println("ticket booked  ------- "+ flightid );
	}
	
	@GetMapping("/ticket/{pnr}")
	void ticketDetailsBasedOnPnr(@PathVariable Integer pnr) {
		System.out.println("ticket details ------- "+ pnr);
	}
	
	@GetMapping("/booking/history/{email}")
	void ticketDetailsBasedOnEmail(@PathVariable Integer email) {
		System.out.println("ticket Details  ------- "+ email);
	}
	
	
	@DeleteMapping("/booking/cancel/{pnr}")
	void cancelTicket(@PathVariable Integer pnr) {
		System.out.println("cancel ticket" +pnr);
		
	}
	
	
	

}



package com.example.demo;

public class Flight {

	String flightNumber ;
	String airline;
	String startingPoint;
	String destination;
	String startTime;
	String scheduledDays;
	String instrumentUsed;
	int totalnumberofbusinessClassSeats;
	int totalNumberOfNonBbusinessClassSeats;
	int totalCost;
	
	@Override
	public String toString() {
		return "Flight [flightNumber=" + flightNumber + ", airline=" + airline + ", startingPoint=" + startingPoint
				+ ", destination=" + destination + ", startTime=" + startTime + ", scheduledDays=" + scheduledDays
				+ ", instrumentUsed=" + instrumentUsed + ", totalnumberofbusinessClassSeats="
				+ totalnumberofbusinessClassSeats + ", totalNumberOfNonBbusinessClassSeats="
				+ totalNumberOfNonBbusinessClassSeats + ", totalCost=" + totalCost + "]";
	}
	
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getAirline() {
		return airline;
	}
	public void setAirline(String airline) {
		this.airline = airline;
	}
	public String getStartingPoint() {
		return startingPoint;
	}
	public void setStartingPoint(String startingPoint) {
		this.startingPoint = startingPoint;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getScheduledDays() {
		return scheduledDays;
	}
	public void setScheduledDays(String scheduledDays) {
		this.scheduledDays = scheduledDays;
	}
	public String getInstrumentUsed() {
		return instrumentUsed;
	}
	public void setInstrumentUsed(String instrumentUsed) {
		this.instrumentUsed = instrumentUsed;
	}
	public int getTotalnumberofbusinessClassSeats() {
		return totalnumberofbusinessClassSeats;
	}
	public void setTotalnumberofbusinessClassSeats(int totalnumberofbusinessClassSeats) {
		this.totalnumberofbusinessClassSeats = totalnumberofbusinessClassSeats;
	}
	public int getTotalNumberOfNonBbusinessClassSeats() {
		return totalNumberOfNonBbusinessClassSeats;
	}
	public void setTotalNumberOfNonBbusinessClassSeats(int totalNumberOfNonBbusinessClassSeats) {
		this.totalNumberOfNonBbusinessClassSeats = totalNumberOfNonBbusinessClassSeats;
	}
	public int getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(int totalCost) {
		this.totalCost = totalCost;
	}
}



